/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file    Stack.h
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <yname@missouristate.edu>
 *          Partner Name <pname@missouristate.edu>
 * @brief   Stack interface.
 */

#ifndef LAB08_STACK_H
#define LAB08_STACK_H

#include "PrecondViolatedExcept.h"

namespace csc232 {
    /**
     * Stack interface.
     * @tparam T the type of elements stored in this stack
     */
    template<typename T>
    class Stack {
    public:
        /**
         * Determines whether this stack is empty.
         *
         * @return True if this stack is empty, false otherwise.
         */
        virtual bool isEmpty() const = 0;

        /**
         * Adds a new entry to the top of this stack.
         *
         * @post If the operation was successful, newEntry is at the top of the stack.
         * @param newEntry The object to be added as a new entry.
         * @return True if the addition is successful or false if not.
         */
        virtual bool push(const T &newEntry) throw(PrecondViolatedExcept) = 0;

        /**
         * Removes the top of this stack.
         *
         * @post If the operation was successful, the top of the stack has been removed.
         * @return True if the removal is successful or false if not.
         */
        virtual bool pop() = 0;

        /**
         * Returns a copy of the top of this stack.
         *
         * @pre The stack is not empty.
         * @post A copy of the top of the stack has been returned, and the stack is unchanged.
         * @return A copy of the top of the stack.
         */
        virtual T peek() const throw(PrecondViolatedExcept) = 0;

        /**
         * Destroys this stack and frees its assigned memory.
         */
        virtual ~Stack() { /* no op */ }
    }; // class Stack

} // end namespace csc232
#endif //LAB08_STACK_H
