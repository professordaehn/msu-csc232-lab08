/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    RPNCalculatorTest.h
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 * @brief   Circle CPPUNIT test specification. DO NOT MODIFY THE CONTENTS OF THIS FILE!
 * @see     http://sourceforge.net/projects/cppunit/files
 */
#ifndef LAB08_RPNCALCULATORTEST_H
#define LAB08_RPNCALCULATORTEST_H

#include <cppunit/extensions/HelperMacros.h>

class RPNCalculatorTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(RPNCalculatorTest);
        CPPUNIT_TEST(testSimpleAddition);
        CPPUNIT_TEST(testSimpleSubtraction);
        CPPUNIT_TEST(testSimpleMultiplication);
        CPPUNIT_TEST(testSimpleDivision);
        CPPUNIT_TEST(testTwoOperations);
        CPPUNIT_TEST(testExpressionWithLeadingWhiteSpace);
        CPPUNIT_TEST(testExpressionWithTrailingWhiteSpace);
        CPPUNIT_TEST(testExpressionWithEmbeddedTabs);
        CPPUNIT_TEST(testExpressionWithEmbeddedNewLine);
        CPPUNIT_TEST(testExpressionWithMixedWhiteSpace);
    CPPUNIT_TEST_SUITE_END();

public:
    RPNCalculatorTest();
    virtual ~RPNCalculatorTest() {}
    void setUp();
    void tearDown();
    void testSimpleAddition();
    void testSimpleSubtraction();
    void testSimpleMultiplication();
    void testSimpleDivision();
    void testTwoOperations();
    void testExpressionWithLeadingWhiteSpace();
    void testExpressionWithTrailingWhiteSpace();
    void testExpressionWithEmbeddedTabs();
    void testExpressionWithEmbeddedNewLine();
    void testExpressionWithMixedWhiteSpace();
};

#endif //LAB08_RPNCALCULATORTEST_H
