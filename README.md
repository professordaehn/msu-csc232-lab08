# Lab08 - Introduction to Stacks

## Getting Started

As usual:

* Fork this repo into your own, private, repo.
* Clone your private repo
* Checkout the develop branch before you begin your coding
* Commit, minimally, after implementing each of the three new operations
* After your final commit, create a pull request that seeks to merge _your_ develop branch into _your_ master branch
* Submit the URL of this pull request as a text submission for the associated assignment on Blackboard

## Goal

Follow the directions in `src/main/Main.cpp`.

## Generating Projects

As usual, this repo contains a number of `cmake` generator options. For example, navigate to the `build/unix` folder and run `cmake` to generate Unix make files that can be used to easily create executables from the source code.

```bash
$ cd build/unix
$ cmake -G "Unix Makefiles" ../..
... lots of output ...
$ make
... lots of output ...
$ ./Lab08
... lab executable output ...
$ ./Lab08Test
... lots of output from the unit tests
$
```
